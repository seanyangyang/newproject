var express = require('express');
var http=require("http");
var url=require("url");
var bodyParser = require('body-parser');
var path = require('path')
var db=require("./config/mysql.js");

var app = express();



app.use(bodyParser.urlencoded({extended:false}));

app.use(express.static('./public'));

app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header("Access-Control-Allow-Methods","PUT,POST,GET,DELETE,OPTIONS");
    res.header("X-Powered-By",' 3.2.1');
    res.header("Content-Type", "application/json;charset=utf-8");
    next();
});

var request=require("./config/request.js");

request(app);

app.listen(8090,function(){
	console.log('start')
});




