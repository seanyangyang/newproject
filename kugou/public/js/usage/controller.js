function guideStartCon($scope,$location,$timeout)
{
	$scope.goGuide=function()
	{
		$location.path("/guide");

	}
	$timeout(function(){
		$location.path("/guide");
	},4000)

}
function guideCon($scope,$location)
{
	$scope.goLogin=function()
	{
		$location.path("/login");
	};
	$scope.goRegister=function()
	{
		$location.path("/register");
	}
}

function singerCon($scope,$http)
{
	var singerSwiper=new Swiper('.swiper-container',{
		onSlideChangeStart:function(swiper)
		{
			$("#singer-nav").find("li").eq(swiper.activeIndex).addClass("singer-active").siblings().removeClass();

		}
	});

	var singerScroll=new IScroll("#singer-scroll");
	$http.get("./mock/singer.json")
		.success(function(res){
			$scope.needH=res.songList.length*45;
			$scope.songList=res.songList;
		});

	$http.get("http://169.254.82.67:8090/singer")
		.success(function(res){
			console.log(res)
			$scope.special=res;
		})

}

function loginCon($scope,$location,$http)
{
	var ls=window.localStorage;
	if(ls.getItem("user") && ls.getItem("pwd"))
	{
		$scope.user=ls.getItem("user");
		$scope.pwd=ls.getItem("pwd");
		$scope.bool=true;
	}
	$scope.backGuide=function(){
		$location.path("/guide");
	};
	$scope.checkLogin=function()
	{
		if($scope.bool)
		{
			ls.setItem("user",$scope.user);
			ls.setItem("pwd",$scope.pwd);
		}
		else
		{
			ls.clear();
		}
		$http.post("http://169.254.82.67:8090/login",{username:$scope.user,userpwd:$scope.pwd})
			.success(function(res){
				if(res=="1")
				{
					$location.path("/index/index-home");
				}
			})
	}


}
function registerCon($scope,$location,$http)
{

	$scope.backGuide=function(){
		$location.path("/guide");
	};
	//判断注册页手机号
	$scope.checkRegister=function(){
		reg = /^1[345678][0-9]{9}/g;
		var strLen = $scope.names.length;
		if( (strLen>=6 && strLen<=12) && $scope.pwd!='' && reg.test($scope.phones)){
			$http.post("http://169.254.82.67:8090/register",{username:$scope.names,userpwd:$scope.pwd,phone: $scope.phones})
				.then(function(res){
					//console.log(res.data)  0或1
					if(res.data==1){
						$location.path("/login"); //注册成功跳到登录页
					}
				},function(error){
					console.log(error)
				})
		}else{
			addEvent();//注册失败的出现遮罩层和弹框
		}

	};

	//遮罩层的函数 和弹框
	function addEvent(){
		if($('#mark').length==0){
			var mark=$('<div class="song_mark"></div>');
			mark.attr('id','mark').appendTo($('body'));
		}
		if($('#hide').length==0){
			var hide='<div class="hint" id="hint">'
				+'<p>注册</p><span>注册失败</span>'
				+'<div class="ok"><i class="tur">确定</i><i class="cancel">取消</i></div>'
				+'</div>';

			$(hide).appendTo($('body'));
			$('body').on('click','.cancel,.tur',function () {
				$('#hint').remove();
				$('#mark').remove()
			})
		}
	}

}


function indHome($timeout) {
	new Swiper('.index-bannerHome', {
		direction: 'horizontal',
		autoplay: 1000,
		loop: false
	});

	var myScroll = new IScroll('.content');
	$timeout(function () {
		myScroll.refresh();
	}, 10)

}

function indexCon($q,$rootScope,$http){

	var defer=$q.defer();
	$http.get("http://169.254.82.67:8090/play").success(function(res)
	{
		defer.resolve(res);
	})
	defer.promise.then(function(data)
	{
		$rootScope.musicArr=data[0];
		$rootScope.goPlay=function(res)
		{
			var music=document.getElementById("music");
			if(music.paused)
			{
				music.play();
				$("#musicPlay").find("i").eq(1).hide().siblings().show();
			}
			else
			{
				music.pause();
				$("#musicPlay").find("i").eq(0).hide().siblings().show();;
			}
		}
		$rootScope.goNext=function()
		{
			var music=document.getElementById("music");
			$rootScope.musicArr=data[1];
			music.play();
			$("#musicPlay").find("i").eq(0).hide().siblings().show();
		}
		$rootScope.goPrev=function()
		{
			var music=document.getElementById("music");
			$rootScope.musicArr=data[2];
			music.play();
			$("#musicPlay").find("i").eq(0).hide().siblings().show();
		}
	})

}

function indRe($timeout){
	new Swiper('.index-banner',{
		direction:'horizontal',
		autoplay:1000,
		loop:false
	});
	var myScroll=new IScroll('.content');
	$timeout(function(){
		myScroll.refresh();
	},10)
}
function indClass($timeout){
	var myScroll=new IScroll('.content');
	$timeout(function(){
		myScroll.refresh();
	},10)
}
function indRankCon($timeout){
	var myScroll=new IScroll('.content');
	$timeout(function(){
		myScroll.refresh();
	},10)
}

//给li添加类
function menu($scope){
	var isshow=new Array(7);
	isshow[0]=true;
	$scope.getClass=function (ind) {
		isshow=new Array(7);
		isshow[ind]=true;
		$scope.isshow=isshow;
	};
	$scope.isshow = isshow; //初始化
}

//allsong

function songCon($http,$scope,$location,$q,$rootScope){

	setTimeout(function(){

		new IScroll('.song-scoll');
	},100);


	$http.get('./mock/allsong.json')
		.success(function(result){
			/*angular.forEach(results,function(v,i){


			 })*/

			$scope.result=result;

		});

	new Swiper('#song_swiper',{
		onSlideChangeStart:function(swiper){
			var $li=$('#title li').eq(swiper.activeIndex);
			setFocus($li);
		}

	});
	$scope.matte=function(ind){

		var $li=$('#title li').eq(ind);
		setFocus($li);
		if(ind!=3){
			addEvent(ind,callback);
		}else{
			$location.path('/files');
		}
	}
	$scope.arr=["歌手","歌曲","专辑","文件夹"];
	function callback(num){
		//console.log(num)
	}

	function setFocus($el){
		$el.addClass("active").siblings().removeClass("active");
	}

	function addEvent(ind,callback){
		if($('#song_mark').length==0){
			var mark=$('<div class="song_mark"></div>');
			mark.attr('id','song_mark').appendTo($('body'));
		}
		if($('#song_hide').length==0){
			var arr=['歌手','歌曲','专辑','风格','文件','添加时间'];
			var hide='<div class="song_hide" id="song_hide">'
				+'<p>'+arr[ind]+'排序</p>'
				+'<ul class="sortList">'
			for(var i in arr){
				hide+="<li>"
					+'<span>按<b>'+arr[i]+'</b>排</span>'
					+'<i>&#xe615;</i>'
					+'</li>'
			}
			hide+='</ul></div>'
			$(hide).appendTo($('body'));
			$('.sortList i').css('display','none')
			$('.sortList span').on('click',function(){
				if(!$('.sortList i').length<1){
					$(this).next().addClass('icon1')
						.css('display','block');
				}
				setTimeout(function(){
					$('#song_mark').remove();
					$('#song_hide').remove();
				},500)
				var str=$(this).find('b').text();
				return callback(str)
			})
		}

		return callback();
	}


}


function filesCon($http,$scope,$location){
	$http.get('./mock/allsong.json')
		.success(function(result){
			var results=result.allpicList;
			$scope.results=results[3];
		});
	$scope.addName=function(){}
	$scope.show=true;
}
function scanCon($scope,$location){
	$scope.arr=['不扫描60秒以下的歌曲','不扫描隐藏歌曲','不扫描AMP,MID格式歌曲']
	$scope.checkTo=function(val){
		val=!val
	}
}

function dataFormat(data)
{
	var tempArr=[];
	for(var i=0,len=Math.ceil(data.length/3);i<len;i++)
	{
		tempArr[i]=[];
		tempArr[i].push(data[i*3]);
		data[i*3+1] && tempArr[i].push(data[i*3+1]);
		data[i*3+2] && tempArr[i].push(data[i*3+2]);
	}
	return tempArr;
}


function themeListCon($scope, $http, getHttp) {

	var length = 10;

	getHttp.http($http, "http://169.254.82.67:8090/category?num="+length)
		.then(function (res) {
			$scope.data = res.data;
			$scope.themearr2 = dataFormat($scope.data)
		})
	$scope.$on("ok", function (data) {
		if( data == "shuaxin" ) {
			getHttp.http($http, "http://169.254.82.67:8090/category?num=10")
				.then(function (res) {
					$scope.data = res.data;
					$scope.themearr2 = dataFormat($scope.data)
				})
		}else{
			length+=3;
			getHttp.http($http, "http://169.254.82.67:8090/category?num="+length)
				.then(function (res) {
					$scope.data = res.data;
					$scope.themearr2 = dataFormat($scope.data)

				})
		}
	})
}
function themeReCon($scope, $http, getHttp) {
	var length = 10;

	getHttp.http($http, "http://169.254.82.67:8090/recom?num="+length)
		.then(function (res) {
			$scope.data = res.data;
			$scope.themearr2 = dataFormat($scope.data)
		})
	$scope.$on("ok", function (data) {
		if( data == "shuaxin" ) {
			getHttp.http($http, "http://169.254.82.67:8090/recom?num=10")
				.then(function (res) {
					$scope.data = res.data;
					$scope.themearr2 = dataFormat($scope.data)
				})
		}else{
			length+=3;
			getHttp.http($http, "http://169.254.82.67:8090/recom?num="+length)
				.then(function (res) {
					$scope.data = res.data;
					$scope.themearr2 = dataFormat($scope.data)

				})
		}
	})


}
function themeTopCon($scope, $http, getHttp) {
	var length = 10;

	getHttp.http($http, "http://169.254.82.67:8090/ranking?num="+length)
		.then(function (res) {
			$scope.data = res.data;
			$scope.themearr2 = dataFormat($scope.data)
		})
	$scope.$on("ok", function (data) {
		if( data == "shuaxin" ) {
			getHttp.http($http, "http://169.254.82.67:8090/ranking?num=10")
				.then(function (res) {
					$scope.data = res.data;
					$scope.themearr2 = dataFormat($scope.data)
				})
		}else{
			length+=3;
			getHttp.http($http, "http://169.254.82.67:8090/ranking?num="+length)
				.then(function (res) {
						$scope.data = res.data;
						$scope.themearr2 = dataFormat($scope.data)

				})
		}
	})
}

function searchCon($http,$scope,$location){

	$scope.arr=["歌手","歌曲","专辑","文件夹"];
	setTimeout(function(){
		new IScroll('.song-scoll',{
			direction:'horizontal',
			autoplay:1000,
			loop:false
		});
	},100);
	$http.get('./mock/allsong.json')
		.success(function(result){
			var obj=[]
			var results=result.allpicList;
			var j=0;
			for(var i in results){
				if(i<3){
					for(var k in results[i]){
						obj.push(results[i][k])
					}
				}
			}
			for(var s in obj){
				obj[s].id=j++;
			}
			$scope.obj=obj;
		})
	$scope.textName='';
	$scope.give=function(obj){
		if($scope.textName !=''){
			if(obj.description.toLowerCase().indexOf($scope.textName.toLowerCase()) !=-1){
				return true;
			}else{
				return false;
			}
		}else{
			return true;
		}
	}
}
function musicCon($scope,$stateParams){
	$scope.img=$stateParams.img;
	$scope.des=$stateParams.des;
	$scope.name=$stateParams.name;
}

angular.module("myApp")
	.controller("singerCon",singerCon)
	.controller('indexCon',indexCon)
	.controller('indHomeCon',indHome)
	.controller('indReCon',indRe)
	.controller('indClassCon',indClass)
	.controller('indRankCon',indRankCon)
	.controller('menu',menu)
	.controller("guideCon",guideCon)
	.controller("guideStartCon",guideStartCon)
	.controller("loginCon",loginCon)
	.controller("registerCon",registerCon)
	//allsong
	.controller('songCon',songCon)
	.controller('filesCon',filesCon)
	.controller('scanCon',scanCon)

	.controller('themeListCon',themeListCon)
	.controller('themeReCon',themeReCon)
	.controller('themeTopCon',themeTopCon)
	.controller('searchCon',searchCon)
	.controller('musicCon',musicCon)

