angular.module("myApp")
    .directive("swiper", function () {
        return {
            restrict: "C",
            link: function (scope, ele, attrs) {
                var themeis = new IScroll(".theme-contine", {
                    probeType: 3,
                    mouseWheel: true,
                    click: true
                });
                var driflag = false;

                setTimeout(function () {
                    themeis.refresh();
                }, 100);

                themeis.on("scroll", function () {
                    if (this.y > 40) {
                        $("#PullDown").show();

                        driflag = true;
                    }
                    if (this.y < this.maxScrollY - 40) {
                        $("#PullUp").show();
                        themeis.refresh();
                        driflag = true;
                    }

                });
                themeis.on("scrollEnd", function () {
                    if (driflag) {

                        var dertimer = this.y
                        setTimeout(function () {

                            if (dertimer == 0) {
                                $("#PullDown").hide();
                                setTimeout(function () {
                                    scope.$emit("ok", "shuaxin");
                                }, 900)
                            } else {
                                $("#PullUp").hide();
                                themeis.refresh();
                                setTimeout(function () {
                                    scope.$emit("ok", "jiazai");
                                }, 900)
                            }
                        }, 1000)
                    }
                    driflag = false;


                });
            }
        }
    })
    .factory("getHttp", function () {
        return {
            http: function ($http, url) {
                return $http.get(url)

            }
        }

    })





