function config($stateProvider,$urlRouterProvider)
{
	$urlRouterProvider
		.otherwise("guideStart");
	$stateProvider
		.state("guideStart",{
			url:"/guideStart",
			templateUrl:"./tpls/guide-start.html",
			controller:"guideStartCon"
		})
		.state("guide",{
			url:"/guide",
			templateUrl:"./tpls/guide.html",
			controller:"guideCon"
		})
		.state("login",{
			url:"/login",
			templateUrl:"./tpls/login.html",
			controller:"loginCon"
		})
		.state("register",{
			url:"/register",
			templateUrl:"./tpls/register.html",
			controller:"registerCon"

		})

		//index
		.state("index",{
			url:"/index",
			templateUrl:"./tpls/index.html",
			controller:"indexCon"
		})
		.state("index.index-home",{
			url:"/index-home",
			templateUrl:"./tpls/index-home.html",
			controller:'indHomeCon'
		})
		.state("index.index-Recommend",{
			url:"/index-Recommend",
			templateUrl:"./tpls/index-Recommend.html",
			controller:'indReCon'
		})
		.state("index.index-Ranking",{
			url:"/index-Ranking",
			templateUrl:"./tpls/index-Ranking.html",
			controller:'indRankCon'
		})
		.state("index.index-classification",{
			url:"/index-classification",
			templateUrl:"./tpls/index-classification.html",
			controller:'indClassCon'
		})
		.state("menu",{
			url:"/menu",
			templateUrl:"./tpls/menu.html",
			controller:'menu'
		})
		.state("singer",{
			url:"/singer",
			templateUrl:"./tpls/singer.html",
			controller:"singerCon"
		})
		//allsong
		.state("song",{
			url:"/song",
			templateUrl:"./tpls/song.html",
			controller:'songCon'
		})
		.state("files",{
			url:"/files",
			templateUrl:"./tpls/files.html",
			controller:'filesCon'
		})
		.state("scan",{
			url:"/scan",
			templateUrl:"./tpls/scan.html",
			controller:'scanCon'
		})

		.state("music",{     
			url:"/music/:img/:des/:name",
			templateUrl:"./tpls/music.html",
			controller:'musicCon'
		})
		.state("wifi",{
			url:"/wifi",
			templateUrl:"./tpls/wifi.html"
		})
		.state("theme",{
			url:"/theme",
			templateUrl:"./tpls/theme.html"
		})
		//排行路由
		.state("theme.top",{
			url:"/top",
			templateUrl:"./tpls/thetop.html",
            controller:"themeTopCon"
		})
		//列表路由
		.state("theme.list",{
			url:"/list",
			templateUrl:"./tpls/thelist.html",
			controller:"themeListCon"
		})
		//推荐路由
		.state("theme.recom",{
			url:"/recom",
			templateUrl:"./tpls/therecom.html",
            controller:"themeReCon"
		})	
		.state("search",{
			url:"/search",
			templateUrl:"./tpls/search.html",
			controller:'searchCon'
		})
}
angular.module("myApp")
	   .config(config)