//var express=require("express");
var db=require("./mysql.js");
var singer=require("./singer.js");
var login=require("./login.js");
var register=require("./register.js");
var recom=require("./recom.js");
var ranking=require("./ranking.js");
var category=require("./category.js");
var play=require("./play.js");
//var route=express.Router();
module.exports=function(app)
{
	app.get("/singer",singer)

	app.post("/login",login)
	app.post("/register",register)
	//百变主题的请求
	//推荐页面
	app.get("/recom",recom)
	//排行页面
	app.get("/ranking",ranking)
	//分类页面
	app.get("/category",category)


	app.get("/play",play)
	
}
